package com.system;
public abstract class Passenger {
	
	   public abstract double travelCost(double rateFactor, int value,boolean frequent);
	   
	   
	   public int getNewspaper(Boolean requested) {
		   if(requested) {
				return 1;
			}
			return 0;
		   
	   }
	}
