package com.system;

import java.util.ArrayList;
import java.util.List;

public class Travellers{
	
	   private double rate;
	   private int destination;
	   private boolean frequent;
	   private boolean news;
	   private boolean meals;
	   private Passenger pass;
	   private List<Travellers> emp= new ArrayList<Travellers>();
	   
	   public Meals mealsCount = new Vacationers();
	   
	   public Travellers() {
		   super();
	   }
	   
	   public Travellers(double rate, final int destination,boolean frequent,boolean news,String PassengerType) {
		   	 super();
		   	 this.setRate(rate);  
		     this.setDestination(destination);
		     this.setFrequent(frequent);
		     this.setNews(news);
		     this.setPass(gettype(PassengerType));
		     //this.setMeals(false);
	   }

	   public Travellers(double rate, int destination,boolean frequent,boolean news,String PassengerType,boolean meals) {
		   	 super();
		   	 this.setRate(rate);  
		     this.setDestination(destination);
		     this.setFrequent(frequent);
		     this.setNews(news);
		     this.setPass(gettype(PassengerType));
		     this.setMeals(meals);
	   }
	   
	   public double getRate() {
			return rate;
		}

		public void setRate(double rate) {
			this.rate = rate;
		}

		public int getDestination() {
			return destination;
		}

		public void setDestination(int destination) {
			this.destination = destination;
		}

		public boolean isFrequent() {
			return frequent;
		}

		public void setFrequent(boolean frequent) {
			this.frequent = frequent;
		}

		public boolean isNews() {
			return news;
		}

		public void setNews(boolean news) {
			this.news = news;
		}

		public Passenger getPass() {
			return pass;
		}

		public void setPass(Passenger pass) {
			this.pass = pass;
		}


	   public double travelCostCalc() {
		   return pass.travelCost(rate,destination,frequent);
	   }
	   
	   public int newsPaperCalc() {
		   return pass.getNewspaper(news);
	   }
	   
	   public int mealsCalc() {
		   if(isMeals()) {
			   return mealsCount.calcMeals(getDestination());
		   }
		   return 0;
	   }
	   
	   public Passenger gettype(String PassengerType){
     	
	      if(PassengerType.equalsIgnoreCase("commuters")){
	         return new Commuters();
	         
	      } else if(PassengerType.equalsIgnoreCase("vacationers")){
	         return new Vacationers();
	         
	      }
	      return null;
	   }
	   
	   public List<Travellers> getItems() {
		   return emp;
		}
		
		public void setItems(List<Travellers> employees) {
		    this.emp = employees;
		}

		public boolean isMeals() {
			return meals;
		}

		public void setMeals(boolean meals) {
			this.meals = meals;
		}

		@Override
		public String toString() {
			return "Traveller [rate=" + rate + ", destination=" + destination + ", frequent=" + frequent + ", news="
					+ news + ", meals=" + meals + ", pass=" + pass + ", emp=" + emp + ", m=" + mealsCount + "]";
		}
	}
