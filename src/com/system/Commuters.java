package com.system;

public class Commuters  extends Passenger {
	   @Override
	   public double travelCost(double rateFactor, int stops,boolean frequent) {
		   
		   double cost,discount;
		   cost = rateFactor*stops;
		   
		   if(frequent) {   
			   discount = (cost * 10)/100;
			   return cost-discount;
		   }
		   
		   return cost;
	   }
}
