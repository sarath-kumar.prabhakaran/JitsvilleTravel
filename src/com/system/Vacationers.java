package com.system;

public class Vacationers extends Passenger implements Meals {
	
		public Vacationers(){
		}
	
	   @Override
	   public double travelCost(double rateFactor, int miles,boolean frequent) {
		   
		   if(miles<5 || miles>4000) {
		   throw new IllegalArgumentException("Miles not Valid");}
		   
		   return rateFactor*miles;
	   }
	   
	@Override
	public int calcMeals(int miles) {
		int count=0;
		
		if(miles<100) {
			return 1;
		}else {
			count = miles/100;
			return count+1;
		}	
	}   
}
