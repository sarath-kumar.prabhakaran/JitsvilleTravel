package com.system;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class PatternTest {
	
	private Travellers com1,com2,com3,vacc1,vacc2,vacboundry1,vacboundry2,vacboundry3,vacboundry4;
	
	@Before
	public void Start() {
		
		com1 = new Travellers(0.5,3,true,false,"commuters");
		com2 = new Travellers(0.5,5,true,false,"commuters");
		com3 = new Travellers(0.5,4,false,true,"commuters");
		
		vacc1 = new Travellers(0.5,90,false,false,"vacationers",true);
		vacc2 = new Travellers(0.5,199,false,true,"vacationers",true);
		
		vacboundry1 = new Travellers(0.5,4001,false,false,"vacationers",true);
		vacboundry2 = new Travellers(0.5,4,false,false,"vacationers",true);
		vacboundry3 = new Travellers(0.5,6,false,false,"vacationers",true);
		vacboundry4 = new Travellers(0.5,3999,false,false,"vacationers",true);
		
		
	}

	@Test
	public void testcommuters() {
	      double expected1 = com1.travelCostCalc();
	      double expected2 = com2.travelCostCalc();
	      double expected3 = com3.travelCostCalc();
	      
	      assertEquals(1.35, expected1,0.001);
	      assertEquals(2.25, expected2,0.001);
	      assertEquals(2.00, expected3,0.001);
	      
	}
	
	@Test
	public void testvacationers() {
	      double expected1 = vacc1.travelCostCalc();
	      double expected2 = vacc2.travelCostCalc();
	      
	      assertEquals(45.0, expected1,0.001);
	      assertEquals(99.5, expected2,0.001);
	      
	}
	
	@Rule public ExpectedException thrownfail1 = ExpectedException.none();
	@Test
	public void testvacationersExceptionfail1() {
		thrownfail1.expect(IllegalArgumentException.class);
		thrownfail1.expectMessage("Miles not Valid");
		double expwillOccur = vacboundry1.travelCostCalc();
	}
	
	@Rule public ExpectedException thrownfail2 = ExpectedException.none();
	@Test
	public void testvacationersExceptionfail2() {
		thrownfail2.expect(IllegalArgumentException.class);
		thrownfail2.expectMessage("Miles not Valid");
		double expwillOccur = vacboundry2.travelCostCalc();
	}
	
	@Test
	public void testvacationersExceptionpass1() {
		double expOccured = vacboundry3.travelCostCalc();
		assertEquals(3.00, expOccured,0.001);
	}
	
	@Test
	public void testvacationersExceptionpass2() {
		double expOccured = vacboundry4.travelCostCalc();
		assertEquals(1999.50, expOccured,0.001);
	}
	
	
	@Test
	public void newsPaperCalccom() {
		double expOccured = com1.travelCostCalc();
		assertEquals(1.35, expOccured,0.001);
	}
	
	@Test
	public void newsPaperCalcvac() {
		double expOccured = vacc1.travelCostCalc();
		assertEquals(45.00, expOccured,0.001);
	}
	
	@Test
	public void mealsCalc() {
		double expOccured = vacc2.mealsCalc();
		assertEquals(2, expOccured,0.001);
	}
	
	@Test
	public void systemReqs() {
		
		Travellers t = new Travellers();
		t.setItems(new ArrayList<Travellers>());
		
		t.getItems().add(new Travellers(0.5,3,true,false,"commuters"));
		t.getItems().add(new Travellers(0.5,5,true,false,"commuters"));
		t.getItems().add(new Travellers(0.5,4,false,true,"commuters"));
		
		t.getItems().add(new Travellers(0.5,90,false,false,"vacationers",true));
		t.getItems().add(new Travellers(0.5,199,false,true,"vacationers",true));
		
		
		double travelCostCalc=0;
		int newsPaperCalc=0;
		int mealsPaperCalc=0;
		
		for(Travellers emp : t.getItems())
	    {
			travelCostCalc += emp.travelCostCalc();
			newsPaperCalc += emp.newsPaperCalc();
			mealsPaperCalc += emp.mealsCalc();

	    }
		
		int noofPassengers = t.getItems().size();
			
		  assertEquals(5, noofPassengers);
		  assertEquals(150.1, travelCostCalc,0.001);
	      assertEquals(2, newsPaperCalc);
	      assertEquals(3, mealsPaperCalc);
	      
	}
	
	@Test
	public void setstopsChange() {
		
		Travellers stopsChange = new Travellers(0.5,3,true,false,"commuters");
		stopsChange.setDestination(10);
		
		assertEquals(10, stopsChange.getDestination());
	}
	
	@Test
	public void setmilesChange() {
		
		Travellers milesChange = new Travellers(0.5,2000,false,false,"vacationers",true);
		milesChange.setDestination(2500);
		
		assertEquals(2500, milesChange.getDestination());
	}

}
